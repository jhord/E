var E = _.cc.carbon.E;

QUnit.test('globals', function(assert) {
  var qunit = E.$('qunit');

  assert.ok(         qunit.e.id === 'qunit',         'by id');
  assert.ok(E.$find('div').e.id === 'qunit',         '$find');
  assert.ok(E.$last('div').e.id === 'qunit-fixture', '$last');

  assert.ok(E.$all('div').length() === document.querySelectorAll('div').length, '$all');
});

QUnit.test('text node', function(assert) {
  var node = E.$text('Text');
  assert.ok(node.e instanceof Text, 'text node');
  assert.ok(node.e.wholeText === 'Text', 'text node inner');
});

QUnit.test('comment node', function(assert) {
  var node = E.$_$('Comment');
  assert.ok(node.e instanceof Comment, 'comment node');
  assert.ok(node.e.textContent === 'Comment', 'comment node inner');
});

QUnit.test('tags', function(assert) {
  Object.keys(E.tags).forEach(function(tag) {
    var e = E[tag].call(E, tag);

    assert.ok(e.e instanceof HTMLElement, 'element: '+ tag);
    assert.ok(e.e.tagName.toLowerCase() === tag.toLowerCase(), 'tag: '+ tag);

    if(tag.toLowerCase() != 'br') {
      assert.ok(e.e.innerText.toLowerCase() === tag.toLowerCase(), 'text: '+ tag);
    }

    var a = E.tags[tag];
    if(typeof a !== 'undefined') {
      a.forEach(function(attr) {
        assert.ok(typeof e['$'+ attr] === 'function', 'attr: '+ tag+ ': '+ attr);
      });
    }
  });
});
