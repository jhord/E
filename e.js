'use strict';

(function(e) {
  var _ = (typeof window === 'undefined' ? process._ : window._);
  _._('cc.carbon.E', e);

  if(typeof exports !== 'undefined') {exports.e = e;}

  // e.e
  e. $find=   $find;
  e. $    =   $    ;
  e. $last=   $last;
  e. e        ;
  e. $all   = $all
  ;  ;        ;

  function $find(query) {
    var q = document.querySelector(query);
    var e = _E(q);
    return e;
  }

  function $(id) {
    var e = _E(document
    .getElementById(id));
    return e;
  }

  function $last(query) {
    var q = document.querySelectorAll(query);
    var e = _E(q[q.length - 1]);
    return e;
  };

  e.$text = function(text) {return _E(document.createTextNode(text));}
  e.$_$   = function(text) {return _E(document.createComment(text) );}

  function $all (query) {
    return         _E(

    document

    .querySelectorAll(query));
  };

/*
  gross globals
*/

  // eww
  var _tags = {
    a              : ['href', 'target', 'download', 'rel', 'hreflang', 'type'],
    abbr           : ['title'],
    address        : undefined,
    area           : [
                      'alt', 'coords', 'download', 'href', 'hreflang',
                      'rel', 'shape', 'target', 'type'
                     ],
    article        : undefined,
    aside          : undefined,
    audio          : [
                      'src', 'crossorigin', 'preload', 'autoplay',
                      'mediagroup', 'loop', 'muted', 'controls'
                     ],
    base           : undefined,
    bdi            : ['dir'],
    bdo            : ['dir'],
    blockquote     : ['cite'],
    br             : undefined,
    button         : [
                      'autofocus', 'disabled', 'form', 'formaction',
                      'formenctype', 'formmethod', 'formnovalidate',
                      'formtarget', 'menu', 'name', 'type', 'value'
                     ],
    canvas         : ['width', 'height'],
    caption        : undefined,
    cite           : undefined,
    code           : undefined,
    col            : ['span'],
    colgroup       : ['span'],
    data           : ['value'],
    datalist       : undefined,
    dd             : undefined,
    del            : ['cite', 'datetime'],
    details        : undefined,
    dfn            : ['title'],
    dialog         : undefined,
    div            : undefined,
    dl             : undefined,
    dt             : undefined,
    em             : undefined,
    embed          : ['src', 'type', 'width', 'height'],
    fieldset       : ['disabled', 'form', 'name'],
    figcaption     : undefined,
    figure         : undefined,
    footer         : undefined,
    form           : [
                      'action', 'autocomplete', 'enctype', 'method', 'name',
                      'novalidate', 'target'
                     ],
    h1             : undefined,
    h2             : undefined,
    h3             : undefined,
    h4             : undefined,
    h5             : undefined,
    h6             : undefined,
    head           : undefined,
    header         : undefined,
    hr             : undefined,
    iframe         : ['src', 'srcdoc', 'name', 'sandbox', 'width', 'height'],
    img            : [
                      'alt', 'src', 'crossorigin', 'usemap', 'ismap',
                      'width', 'height'
                     ],
    input          : [
                      'accept', 'alt', 'autocomplete', 'autofocus',
                      'checked', 'dirname', 'disabled',
                      'form', 'formaction', 'formenctype', 'formmethod',
                      'formnovalidate', 'formtarget', 'height', 'inputmode',
                      'list', 'max', 'maxlength', 'min', 'minlength',
                      'multiple', 'name', 'pattern', 'placeholder',
                      'readonly', 'required', 'size', 'src', 'step',
                      'title', 'type', 'value', 'width'
                     ],
    ins            : ['cite', 'datetime'],
    kbd            : undefined,
    label          : ['form', 'for'],
    legend         : undefined,
    li             : ['value'],
    link           : undefined,
    main           : undefined,
    map            : ['name'],
    mark           : undefined,
    menu           : undefined,
    menuitem       : undefined,
    meta           : undefined,
    meter          : ['value', 'min', 'max', 'low', 'high', 'optimum'],
    nav            : undefined,
    noscript       : undefined,
    object         : [
                       'data', 'type', 'typemustmatch', 'name', 'usemap',
                       'form', 'width', 'height'
                     ],
    ol             : ['reversed', 'start', 'type'],
    optgroup       : ['disabled', 'label'],
    option         : ['disabled', 'label', 'selected', 'value'],
    output         : ['for', 'form', 'name'],
    p              : undefined,
    param          : ['name', 'value'],
    pre            : undefined,
    progress       : ['value', 'max'],
    q              : ['cite'],
    rb             : undefined,
    rp             : undefined,
    rt             : undefined,
    rtc            : undefined,
    ruby           : undefined,
    s              : undefined,
    samp           : undefined,
    script         : [
                      'src', 'type', 'charset', 'async',
                      'defer', 'crossorigin'
                     ],
    section        : undefined,
    select         : [
                       'autofocus', 'disabled', 'form', 'multiple', 'name',
                       'required', 'size'
                     ],
    small          : undefined,
    source         : ['src', 'type'],
    span           : undefined,
    strong         : undefined,
    style          : undefined,
    sub            : undefined,
    summary        : undefined,
    sup            : undefined,
    table          : ['border', 'sortable'],
    tbody          : undefined,
    td             : ['coslpan', 'rowspan', 'headers'],
    template       : undefined,
    textarea       : [
                       'autocomplete', 'autofocus', 'cols', 'dirname',
                       'disabled', 'form', 'inputmode', 'maxlength',
                       'minlength', 'name', 'placeholder', 'readonly',
                       'required', 'rows', 'wrap'
                     ],
    tfoot          : undefined,
    th             : [
                       'colspan', 'rowspan', 'headers',
                       'scope', 'abbr', 'sorted'
                     ],
    thead          : undefined,
    time           : ['datetime'],
    title          : undefined,
    tr             : undefined,
    track          : ['kind', 'src', 'srclang', 'label', 'default'],
    ul             : undefined,
    var            : undefined,
    video          : [
                       'src', 'crossorigin', 'poster', 'preload',
                       'autoplay', 'mediagroup', 'loop', 'muted',
                       'controls', 'width', 'height'
                     ],
    wbr            : undefined,
  };
  Object.keys(_tags).forEach(function (tag) {
    e[tag] = function(content) {
      var e = _E(tag);
      for(var idx = 0, len = arguments.length; idx < len; idx++) {
        var arg = arguments[idx];
        if(typeof arg === 'string') {
          e.e.appendChild(document.createTextNode(arg));
        }
        else {
          e.e.appendChild(n_n(arg));
        }
      }
      return e;
    };
  });
  e.tags = _tags;

  var _g_attr = { };
  [
    'accesskey',
    'class',
    'contenteditable',
    'dir',
    'hidden',
    'id',
    'lang',
    'onabort',
    'onblur',
    'oncancel',
    'oncanplay',
    'oncanplaythrough',
    'onchange',
    'onclick',
    'oncuechange',
    'ondblclick',
    'ondurationchange',
    'onemptied',
    'onended',
    'onerror',
    'onfocus',
    'oninput',
    'oninvalid',
    'onkeydown',
    'onkeypress',
    'onkeyup',
    'onload',
    'onloadeddata',
    'onloadedmetadata',
    'onloadstart',
    'onmousedown',
    'onmouseenter',
    'onmouseleave',
    'onmousemove',
    'onmouseout',
    'onmouseover',
    'onmouseup',
    'onmousewheel',
    'onpause',
    'onplay',
    'onplaying',
    'onprogress',
    'onratechange',
    'onreset',
    'onresize',
    'onscroll',
    'onseeked',
    'onseeking',
    'onselect',
    'onshow',
    'onstalled',
    'onsubmit',
    'onsuspend',
    'ontimeupdate',
    'ontoggle',
    'onvolumechange',
    'onwaiting',
    'role',
    'spellcheck',
    'style',
    'tabindex',
    'title',
    'translate',
  ].forEach(function(attr) {_g_attr['$'+ attr] = a_a(attr);});
  var _g_attr_keys = Object.keys(_g_attr);

  var _g_eff = {
    E:        e,
    $:        e.$,

    empty:   empty,
    body:     body,
    html:     html,

    w:        w,
    y:        y,
    hang:     hang,
    text:     text,
    data:     data,
    a:        a,

    css:     css,
    hide:    hide,
    show:    show,
    vis:     vis,
    dim:     dim,
    width:   width,
    height:  height,
    abs:     abs,
    abs_x:   abs_x,
    abs_y:   abs_y,
    z:       z,

    p:       p,
    s:       s,
    c:       c,
    ls:      ls,
    first:   first,
    last:    last,

    clone:   clone,
    cast:    cast,
    e_:      e_,
    e$:      e$,
    shift:   shift,
    unshift: unshift,
    push:    push,
    pop:     pop,
    bench:   bench,
    press:   press,
    lift:    lift,
    drop:    drop,
    each:    each,
    cp:      cp,
    mv:      mv,
    rm:      rm,

    ear:     ear,
  };
  var _g_eff_keys = Object.keys(_g_eff);
/*
    tree  (L of children
    above (L of those above
    below (L of those below

    splice
    iterator
    map
*/


/****************************************
* infernalinternal hellpers
**/

  function _eff(e) {
    var idx;
    var len;

    // global attributes
    for(idx = 0, len = _g_attr_keys.length; idx < len; idx++) {
      var attr = _g_attr_keys[idx];
      e[attr]  = _g_attr[attr];
    }

    // global functions
    for(idx = 0, len = _g_eff_keys.length; idx < len; idx++) {
      var key = _g_eff_keys[idx];
      e[key]  = _g_eff[key];
    }

    return e;
  }

  function a_a(name) {
    return function (value) {
      var self = this;
      if(arguments.length <= 0) {return self.e.getAttribute(name);}
      if(value === null) {self.e.removeAttribute(name);}
      else               {self.e.setAttribute(name, value);}
      return self;
    }
  }

  function n_n(e) {
    var n;
    if(
         e instanceof D
      || e instanceof E
      || e instanceof F
      || e instanceof L
      || e instanceof N
      || e instanceof S
    ) {
      n = e.e;
    }
    else if(typeof e === 'string') {
      n = document.createTextNode(e);
    }
    else {
      n = e;
    }
    return n;
  }


/*
  BEHOLD(ers)!  DEATH RAYS!
*/

  function D(_D) {
    var self  = this;
    self.e    = _D;
    self.type = 'document';
    return _eff(self);
  }

  function E(_E) {
    var self  = this;
    self.e    = _E;
    self.type = 'element';

    // add attribute handlers
    (function(e) {
      var a = _tags[e.e.tagName.toLowerCase()];
      if(typeof a !== 'undefined') {
        for(var idx = 0, len = a.length; idx < len; idx++) {
          var attr = a[idx];
          e['$'+ attr] = a_a(attr);
        }
      }
    })(self);

    return _eff(self);
  }

  function F(_F) {
    var self  = this;
    self.e    = _F;
    self.type = 'fragment';
    return _eff(self);
  }

  function L(_L) {
    var self  = this;
    self.e    = _L;
    self.type = 'list';

    // FIXME: what else to add here?
    self.each = function(f) {
      var self = this;
      var e    = self.e;
      for(var idx = 0, len = e.length; idx < len; idx++) {
        f.call(self, _E(e[idx]));
      }
      return self;
    }
    self.length = function() {return self.e.length;};
    return self;
  }

  function N(_N) {
    var self  = this;
    self.e    = _N;
    self.type = 'node';
    // FIXME: probably corrupt
    return _eff(self);
  }

  function S(_S) {
    var self  = this;
    self.e    = _S;
    self.type = 'shadow';
    return _eff(self);
  }

  function T(_T) {
    var self  = this;
    self.e    = _T;
    self.type = 'text';
    // FIXME: probably corrupt
    return _eff(self);
  }


/* central */

  function _E(nvelope) {
    var o;

    if(typeof nvelope === 'undefined' || nvelope === null) {
      o = new F(document.createDocumentFragment());
    }
    else if(typeof nvelope === 'string') {
      o = new E(document.createElement(nvelope));
    }
    else if(nvelope instanceof HTMLDocument) {
      o = new D(nvelope);
    }
    else if(nvelope instanceof HTMLElement) {
      o = new E(nvelope);
    }
    else if(nvelope instanceof DocumentFragment) {
      o = new F(nvelope);
    }
    else if(nvelope instanceof ShadowRoot) {
      o = new S(nvelope);
    }
    else if(nvelope instanceof NodeList) {
      o = new L(nvelope);
    }
    else if(nvelope instanceof Text) {
      o = new T(nvelope);
    }
    else if(nvelope instanceof Node) {
      o = new N(nvelope);
    }
    else {
      o = nvelope;
    }

    return o;
  }


/*

.oO( circ.le 8 )Oo.

*/

  // FIXME: input wrappers (radio, submit, reset, etc);

  /*
  ** content
  */

  function w(f) {
    var e = this;
    f.call(e);
    return e;
  }

  function y(f) {
    var e = this;
    return function () {f.apply(e, arguments); return e;};
  }

  function hang(n) {
    var p = n_n(n);
    p.appendChild(this.e);
    return this;
  }

  function empty() {
    var e = this.e;
    while(e.firstChild) {
      e.removeChild(e.firstChild);
    }
    return this;
  }

  function body() {
    this.empty();
    return e$.apply(this, arguments);
  }

  function html(html) {
    if(arguments.length <= 0) {return this.e.innerHTML;}
    this.e.innerHTML = html;
    return this;
  }

  function text(text) {
    if(arguments.length <= 0) {return this.e.textContent;}
    this.e.textContent = text;
    return this;
  }

  function data(name, value) {
    if(arguments.length <= 1) {return this.e.getAttribute('data-'+ name);}
    this.e.setAttribute('data-'+name, value);
    return this;
  }

  function a(name, value) {
    // FIXME: a(Object) a(name, value)
    if(arguments.length <= 1) {return this.e.getAttribute(name);}
    this.e.setAttribute(name, value);
    return this;
  }

  /*
  ** styling/css
  */

  function css(names) {
    // FIXME: can we set style here, too?
    if(arguments.length <= 0) {
      names = this.e.className.split(/ /);
      return names;
    }

    if(names === null) {this.e.removeAttribute('class');}
    else {
      this.e.className = Array.isArray(names)
        ? names.join(' ')
        : names
      ;
    }
    return this;
  }

  function hide() {
    var e = this.e;
    e.style.display = 'none';
    return this;
  }

  function show(how) {
    e.style.display = how;
    return this;
  }

  function vis(flag) {
    var e = this.e;
    if(arguments.length <= 0) {
      return e.style.visibility === 'visible';
    }
    e.style.visibility = flag ? 'visible' : 'hidden';
    return this;
  }

  function dim(width, height) {
    var e = this.e;
    e.style.width  = width .toString(10)+ 'px';
    e.style.height = height.toString(10)+ 'px';
    return this;
  }

  function width(width) {
    var e = this.e;
    if(arguments.length <= 0) {return e.style.width;}
    e.style.width = width.toString(10)+ 'px';
    return this;
  }

  function height(height) {
    var e = this.e;
    if(arguments.length <= 0) {return e.style.height;}
    e.style.height = height.toString(10)+ 'px';
    return this;
  }

  function abs(abs_x, abs_y) {
    var e = this.e;
    e.style.position = 'absolute';
    e.style.left     = abs_x.toString(10)+ 'px';
    e.style.top      = abs_y.toString(10)+ 'px';
    return this;
  }

  function abs_x(abs_x) {
    var e = this.e;
    if(arguments.length <= 0) {return e.style.left;}
    e.style.left = abs_x.toString(10)+ 'px';
    return this;
  }

  function abs_y(abs_y) {
    var e = this.e;
    if(arguments.length <= 0) {return e.style.top;}
    e.style.top = abs_y.toString(10)+ 'px';
    return this;
  }

  function z(abs_z) {
    var e = this.e;
    if(arguments.length <= 0) {return e.style.zIndex;}
    e.style.zIndex = abs_z;
    return this;
  }

  /*
  ** query
  */

  function p() {
    var e = this.e.parentNode;
    return _E(e);
  }

  function s(n) {
    var s = this.e;
    if(n > 0) {
      while(n-- > 0) {
        s = s.nextSibling;
        if(null === s) {break;}
      }

      return _E(s);
    }

    if(n < 0) {
      while(n++ < 0) {
        s = s.previousSibling;
        if(null === s) {break;}
      }

      return _E(s);
    }

    return this;
  }

  function c(n) {
    var e = this.e;
    if(arguments.length <= 0) {
      return e.childNodes.length;
    }
    return _E(e.childNodes[n]);
  }

  function ls(query) {
    var q = typeof query === 'undefined' || query === null ? '*' : query;
    var l = this.e.querySelectorAll(q);
    var e = _E(l);
    return e;
  }

  function first(query) {
    var q = this.e.querySelector(query);
    var e = _E(q);
    return e;
  }

  function last(query) {
    var q = this.e.querySelectorAll(query);
    var e = _E(q[q.length - 1]);
    return e;
  }

  /*
  ** document-object-manipulation
  */

  function clone() {
    var e = this.e;
    return _E(e.cloneNode(true));
  }

  function cast(ghost) {
    var e = this.e;
    return _E(e.attachShadow({mode: ghost ? 'closed' : 'open'}))
  }

  function e_(e) {
    if(Array.isArray(e)) {
      for(var idx = 0, len = e.length; idx < len; idx++) {
        this.e.removeChild(e[idx]);
      }
    }
    else {
      this.e.removeChild(n_n(e));
    }
    return this;
  }

  function e$() {
    for(var adx = 0, alen = arguments.length; adx < alen; adx++) {
      var e = arguments[adx];
      if(Array.isArray(e)) {
        for(var idx = 0, ilen = e.length; idx < ilen; idx++) {
          this.e.appendChild(n_n(e[idx]));
        }
      }
      else {
        this.e.appendChild(n_n(e));
      }
    }
    return this;
  }

  function unshift() {
    var b = this.e.firstChild;
    if(null === b) {
      e$.apply(this, arguments);
      return this;
    }

    for(var adx = 0, alen = arguments.length; adx < alen; adx++) {
      var e = arguments[adx];
      if(Array.isArray(e)) {
        for(var idx = 0, len = e.length; idx < len; idx++) {
          this.e.insertBefore(n_n(e[idx]), b);
        }
      }
      else {
        this.e.insertBefore(n_n(e), b);
      }
    }
    return this;
  }

  function shift(n) {
    var e = this.e;
    if(arguments.length <= 0 || typeof n === 'undefined' || null === n || n < 0) {n=1};
    while(n-- > 0) {
      e.removeChild(e.firstChild);
    }
    return this;
  }

  function push() {
    for(var adx = 0, alen = arguments.length; adx < alen; adx++) {
      var e = arguments[adx];
      if(Array.isArray(e)) {
        for(var idx = 0, len = e.length; idx < len; idx++) {
          this.e.appendChild(n_n(e[idx]));
        }
      }
      else {
        this.e.appendChild(n_n(e));
      }
    }
    return this;
  }

  function pop(n) {
    var e = this.e;
    if(arguments.length <= 0 || typeof n === 'undefined' || null === n || n < 0) {n=1};
    while(n-- > 0) {
      e.removeChild(e.lastChild);
    }
    return this;
  }

  function bench() {
    var p = this.p().e;
    var E = this.e;

    for(var adx = 0, alen = arguments.length; adx < alen; adx++) {
      var e = arguments[adx];
      if(Array.isArray(e)) {
        for(var idx = 0, len = e.length; idx < len; idx++) {
          p.insertBefore(n_n(e[idx]), E);
        }
      }
      else {
        p.insertBefore(n_n(e), E);
      }
    }
    return this;
  }

  function press() {
    var b = this.e.nextSibling;
    if(null === b) {
      e$.apply(this.p(), arguments);
      return this;
    }
    var p = this.p().e;

    for(var adx = 0, alen = arguments.length; adx < alen; adx++) {
      var e = arguments[adx];

      if(Array.isArray(e)) {
        for(var idx = 0, len = e.length; idx < len; idx++) {
          p.insertBefore(n_n(e[idx]), b);
        }
      }
      else {
        p.insertBefore(n_n(e), b);
      }
    }
    return this;
  }

  // FIXME: these don't work on text nodes
  function lift(n) {
    if(n <= 0) {
      this.p().unshift(this.rm());
      return this;
    }
    var s = this.s(-n);
    if(null === s) {
      this.p().unshift(this.rm());
      return this;
    }

    s.bench(this.rm());
    return this;
  }

  // FIXME: these don't work on text nodes
  function drop(n) {
    if(n <= 0) {
      this.p().push(this.rm());
      return this;
    }
    var s = this.s(n);
    if(null === s) {
      this.p().push(this.rm());
      return this;
    }

    s.press(this.rm());
    return this;
  }

  function each(f) {
    var e = this.e;
    var n = e.childNodes;
    for(var idx = 0, len = n.length; idx < len; idx++) {
      f.call(this, _E(n[idx]));
    }
  }

  function cp(d) {
    n_n(d).appendChild(this.clone().e);
    return this;
  }

  function mv(d) {
    n_n(d).appendChild(this.rm().e);
    return this;
  }

  function rm() {
    var e = this.e;
    var p = e.parentNode;

    if(e.parentNode !== null) {
      p.removeChild(e);
    }
    return this;
  }

  /*
  ** events
  */

  // hey!  listen!
  function ear(noise, wax, tin) {
    this.e.addEventListener(noise, wax, tin);
    return this;
  }

})({ });
